Attribute VB_Name = "User32Functions"
Option Explicit
'=========CLIPBOARDFUNCTION==========================
Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) _
   As Long
Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) _
   As Long
Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Long, _
   ByVal dwBytes As Long) As Long
Declare Function CloseClipboard Lib "user32" () As Long
Declare Function OpenClipboard Lib "user32" (ByVal hWnd As Long) _
   As Long
Declare Function EmptyClipboard Lib "user32" () As Long
Declare Function lstrcpy Lib "kernel32" (ByVal lpString1 As Any, _
   ByVal lpString2 As Any) As Long
Declare Function SetClipboardData Lib "user32" (ByVal wFormat _
   As Long, ByVal hMem As Long) As Long
Public Declare Function GetWindow Lib "user32" (ByVal hWnd As Long, _
    ByVal wCmd As Long) As Long
 
Public Const GHND = &H42
Public Const CF_TEXT = 1
Public Const MAXSIZE = 4096
Public Const GW_HWNDNEXT As Long = 2
'===================================

Declare Function apiShowWindow Lib "user32" Alias "ShowWindow" _
            (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long

Global Const O2S_WINDOW_MAX = 3
Global Const O2S_WINDOW_NORMAL = 1
Global Const O2S_WINDOW_MIN = 2

'Get ChildWindows of a main window
    Public Declare Function EnumChildWindows Lib "user32.dll" ( _
    ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long

'Get Class Name of a specfic window
    Private Declare Function GetClassName Lib "user32.dll" Alias "GetClassNameA" _
    (ByVal hWnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
'Access computer memory to find the byte values of a specflic window maybe apply to child window also
    Private Declare Function GetWindowLong Lib "user32.dll" Alias "GetWindowLongA" _
    (ByVal hWnd As Long, ByVal nIndex As Long) As Long
    
Declare Function BringWindowToTop Lib "user32" _
(ByVal lngHWnd As Long) As Long

Declare Function IsWindowVisible Lib "user32" (ByVal hWnd As Long) As Boolean
    
    
Declare Function GetClientRect Lib "user32" (ByVal hWnd As Long, lpRect As RECT) As Long
Declare Function GetParent Lib "user32" (ByVal hWnd As Long) As Long
Declare Function ClientToScreen Lib "user32" (ByVal hWnd As Long, ByRef lpPoint As RECT) As Boolean
'        Public Type RECT
'            Left As Long
'            Top As Long
'            Right As Long
'            Bottom As Long
'        End Type

Declare Function MapWindowPoints Lib "user32" _
(ByVal hwndFrom As Long, _
ByVal hwndTo As Long, lppt As Any, _
ByVal cPoints As Long) As Long

Declare Function ChildWindowFromPoint Lib "user32" _
(hWnd As Long, ByVal xPoint As Long, ByVal yPoint As Long) As Long

Declare Function ChildWindowFromPointEx Lib "user32" _
(ByVal hWnd As Long, ByVal ptx As Long, ByVal pty As Long, ByVal un As Long) As Long
 
'Public Type POINTAPI
'        x As Long
'        y As Long
'End Type

Public Declare Function ExitWindowsEx& Lib "user32" (ByVal uFlags&, ByVal wReserved&)

Public Declare Function SetForegroundWindow Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
Global Const VK_CONTROL = &H11 ' Control Key for Keyboard Event

Global Const VK_0 = &H30
Global Const VK_1 = &H31
Global Const VK_2 = &H32
Global Const VK_3 = &H33
Global Const VK_4 = &H34
Global Const VK_5 = &H35
Global Const VK_6 = &H36
Global Const VK_7 = &H37
Global Const VK_8 = &H38
Global Const VK_9 = &H39
Global Const VK_A = &H41
Global Const VK_B = &H42
Global Const VK_C = &H43
Global Const VK_D = &H44
Global Const VK_E = &H45
Global Const VK_F = &H46
Global Const VK_G = &H47
Global Const VK_H = &H48
Global Const VK_I = &H49
Global Const VK_J = &H4A
Global Const VK_K = &H4B
Global Const VK_L = &H4C
Global Const VK_M = &H4D
Global Const VK_N = &H4E
Global Const VK_O = &H4F
Global Const VK_P = &H50
Global Const VK_Q = &H51
Global Const VK_R = &H52
Global Const VK_S = &H53
Global Const VK_T = &H54
Global Const VK_U = &H55
Global Const VK_V = &H56
Global Const VK_W = &H57
Global Const VK_X = &H58
Global Const VK_Y = &H59
Global Const VK_Z = &H5A


Global Const VK_DOWN = &H28 ' Down for Keyboard Event
Global Const VK_UP = &H26 'UP key
Global Const VK_LEFT = &H25 ' Left key
Global Const VK_RIGHT = &H27 ' Right key
Global Const VK_NUMLOCK = &H90 'Number key
Global Const KEYEVENTF_KEYUP = &H2 ' Keyboard constant for Keyboard Event
Global Const VK_ALT = &H12 ' Left Alt key for keybread event.
Global Const VK_APPS = &H5D
Global Const VK_RETURN = &HD 'Enter key
Global Const VK_SHIFT = &H10 'SHIFT Key
Global Const VK_TAB = &H9  'TAB Key
Global Const VK_SPACE = &H20 'SPACEBAR
Global Const VK_BACK = &H8 'BACK KEY

' Access the GetCursorPos function in user32.dll
      Declare Function GetCursorPos Lib "user32" _
      (lpPoint As POINTAPI) As Long
' Access the GetCursorPos function in user32.dll
      Declare Function SetCursorPos Lib "user32" _
      (ByVal x As Long, ByVal y As Long) As Long
      
'We use GetPixel and GetWindowDC functions for checking right page has been loaded for SRI.
     Declare Function GetPixel Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long) As Long
     Declare Function GetWindowDC Lib "user32" (ByVal hWnd As Long) As Long

      ' GetCursorPos requires a variable declared as a custom data type
      ' that will hold two integers, one for x value and one for y value
       Public Type POINTAPI
           x As Long
           y As Long
        End Type
      
Public Const HWND_TOPMOST = -1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_NOREDRAW = &H8
Private Const kCapital = 20
Private Const kNumlock = 144
      
Public Declare Function SetWindowPos Lib "user32.dll" ( _
     ByVal hWnd As Long, _
     ByVal hWndInsertAfter As Long, _
     ByVal x As Long, _
     ByVal y As Long, _
     ByVal cx As Long, _
     ByVal cy As Long, _
     ByVal wFlags As Long) As Long
Public Const HWND_NOTOPMOST As Long = -2
      
      
Declare Function apimouse_event Lib "user32" Alias "mouse_event" (ByVal dwFlags As Long, _
            ByVal dX As Long, ByVal dY As Long, ByVal cButtons As Long, ByVal dwExtraInfo _
            As Long) As Boolean

Global Const MOUSEEVENTF_MOVE As Long = &H1 ' mouse move

Global Const MOUSEEVENTF_LEFTDOWN As Long = &H2 ' left button down

Global Const MOUSEEVENTF_LEFTUP As Long = &H4 ' left button up

Global Const MOUSEEVENTF_RIGHTDOWN As Long = &H8 ' right button down

Global Const MOUSEEVENTF_RIGHTUP As Long = &H10 ' right button up

Global Const MOUSEEVENTF_MIDDLEDOWN As Long = &H20 ' middle button down

Global Const MOUSEEVENTF_MIDDLEUP As Long = &H40 ' middle button up

Global Const MOUSEEVENTF_ABSOLUTE As Long = &H8000 ' absolute move

Global Const MOUSEEVENTF_WHEEL As Long = &H800 ' wheel button rolled

'Finded window
'Public Const FORMAT_MESSAGE_FROM_SYSTEM As Long = &H1000

Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As Any, _
    ByVal lpWindowName As String) As Long
    
Public Declare Function GetWindowTextW Lib "user32.dll" (ByVal hWnd As Long, ByVal lpString As Long, ByVal nMaxCount As Long) As Long
Public Declare Function GetWindowTextLengthW Lib "user32.dll" (ByVal hWnd As Long) As Long

Public Declare Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" (ByVal hWnd As Long) As Long
    
'for making a window the topmost
'Private Declare Function SetWindowPos Lib "user32" (ByVal HWND As Long, _
'    ByVal hWndInsertAfter As Long, _
'    ByVal x As Long, _
'    ByVal y As Long, _
'    ByVal cx As Long, _
'    ByVal cy As Long, _
'    ByVal wFlags As Long) As Long


Public Declare Function FindWindowExA Lib "user32" _
(ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, _
ByVal lpsz2 As String) As Long


Public Declare Function EnableWindow Lib "user32.dll" (ByVal hWnd As Long, ByVal fEnable As Boolean) As Long

Public Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Public Declare Function GetMenuItemID Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Public Declare Function GetMenuString Lib "user32" Alias "GetMenuStringA" (ByVal hMenu As Long, ByVal wIDItem As Long, ByVal lpString As String, ByVal nMaxCount As Long, ByVal wFlag As Long) As Long


Public Const HWND_TOPMOST1 = -1
Public Const SWP_NOSIZE1 = &H1
Public Const SWP_NOMOVE1 = &H2
Public Const HWND_TOP1 = 0
Public Const HWND_BOTTOM1 = 1
Public Const HWND_NOTOPMOST1 = -2

'getwindowrect
Public Declare Function GetWindowRect Lib "user32" (ByVal hWnd As Long, RECT As RECT) As Long

Public Declare Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Long, ByVal Source As Long, ByVal MessageId As Long, ByVal LanguageId As Long, ByVal Buffer As String, ByVal Size As Long, ByVal Arguments As Long) As Long
Public Type RECT
  Left As Long
  Top As Long
  Right As Long
  Bottom As Long
End Type
Declare Function GetKeyboardState Lib "user32" (pbKeyState As Byte) As Long
Declare Function SetKeyboardState Lib "user32" (kbArray As Byte) As Long
Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Global Const sv_click As Long = &HF5&
Global Const SV_TEXT = &HC
Global Const SV_CHAR = &H102
Global Const SV_COMMAND = &H111
Global Const SV_KEYDOWN = &H290
Global Const CB_GETCURSEL = &H147
Global Const CB_SETCURSEL = &H14E
Global Const CB_SHWODROPDOWN = &H14F
Global Const CB_SELECTSTRING = &H14D
Global Const CBN_SELCHANGE = &H1
Global Const CHILDACTIVATE = &H22
Global Const TCM_SETCURFOCUS = &H1330&

'Global Const _KEYDOWN = &H100

Global Const EM_GETSEL = &HB0
Global Const EM_SETSEL = &HB1
Global Const EM_GETLINECOUNT = &HBA
Global Const EM_LINEINDEX = &HBB
Global Const EM_LINELENGTH = &HC1
Global Const EM_LINEFROMCHAR = &HC9

Global Const BM_GETCHECK = &HF0
Global Const BST_CHECKED = &H1
Global Const BM_CLICK = &HF5




Public Declare Function SendInput Lib "user32.dll" _
 (ByVal nInputs As Long, ByRef pInputs As Any, _
 ByVal cbSize As Long) As Long
 
Public Declare Function VkKeyScan Lib "user32" Alias "VkKeyScanA" _
 (ByVal cChar As Byte) As Integer

'posmouse
Public Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Global Const WM_LBUTTONDOWN = &H201
Global Const WM_LBUTTONUP = &H202
Global Const WM_LBUTTONDBLCLK = &H203
Global Const WM_MOUSEMOVE = &H200
Global Const WM_KEYDOWN = &H100
Global Const WM_KEYUP = &H101
Global Const MOUSEEVENTF_LEFTDOWN1 = &H2
Global Const MOUSEEVENTF_LEFTUP1 = &H4
Global Const WM_ACTIVATE = &H6
Global Const WM_ENABLE = &HA
Global Const WM_SHOWWINDOW = &H18
Global Const WM_CHILDACTIVATE = &H22
Global Const WM_SETCURSOR = &H20
Global Const WM_SETFOCUS = &H7
Global Const WM_KILLFOCUS = &H8
Global Const WM_CLOSE = &H16
Global Const WM_PASTE = &H302
Global Const WM_SIZE = &H5
Global Const WM_GETTEXTLENGTH = &HE
Global Const WM_GETTEXT = &HD

Global Const MK_CONTROL = &H8
Global Const MK_LBUTTON = &H1
Global Const MK_MBUTTON = &H10
Global Const MK_RBUTTON = &H2
Global Const MK_SHIFT = &H4
Global Const MK_XBUTTON1 = &H20
Global Const MK_XBUTTON2 = &H40
'=================

Global Const WM_NCHITTEST = &H84
Public Enum WM_NCHITTEST
    HTBORDER = 18
    HTBOTTOM = 15
    HTBOTTOMLEFT = 16
    HTBOTTOMRIGHT = 17
    HTCAPTION = 2
    HTCLIENT = 1
    HTCLOSE = 20
    HTERROR = -2
    HTGROWBOX = 4
    HTHELP = 21
    HTHSCROLL = 6
    HTLEFT = 10
    HTMENU = 5
    HTMAXBUTTON = 9
    HTMINBUTTON = 8
    HTNOWHERE = 0
    HTREDUCE = HTMINBUTTON
    HTRIGHT = 11
    HTSIZE = HTGROWBOX
    HTSYSMENU = 3
    HTTOP = 12
    HTTOPLEFT = 13
    HTTOPRIGHT = 14
    HTTRANSPARENT = -1
    HTVSCROLL = 7
    HTZOOM = HTMAXBUTTON
End Enum
'===============

Declare Function BlockInput Lib "user32" (ByVal fBlock As Long) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'===============================================================================

      Declare Function MoveWindow Lib "user32" (ByVal hWnd As Long, _
         ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, _
         ByVal nHeight As Long, ByVal bRepaint As Long) As Long

      Declare Function GetDesktopWindow Lib "user32" () As Long

      Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long

      Declare Function EnumThreadWindows Lib "user32" (ByVal dwThreadId _
         As Long, ByVal lpfn As Long, ByVal lParam As Long) As Long

      Declare Function GetWindowThreadProcessId Lib "user32" _
         (ByVal hWnd As Long, lpdwProcessId As Long) As Long

      Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" _
         (ByVal hWnd As Long, ByVal lpString As String, _
         ByVal cch As Long) As Long

      Public TopCount As Integer     ' Number of Top level Windows
      Public ChildCount As Integer   ' Number of Child Windows
      Public ThreadCount As Integer  ' Number of Thread Windows

     Public Function EnumWinProc(ByVal lhWnd As Long, ByVal lParam As Long) _
         As Long
         Dim Retval As Long, ProcessID As Long, ThreadID As Long
         Dim WinClassBuf As String * 255, WinTitleBuf As String * 255
         Dim winClass As String, winTitle As String

         Retval = GetClassName(lhWnd, WinClassBuf, 255)
         winClass = StripNulls(WinClassBuf)  ' remove extra Nulls & spaces
         Retval = GetWindowText(lhWnd, WinTitleBuf, 255)
         winTitle = StripNulls(WinTitleBuf)
         TopCount = TopCount + 1
         ' see the Windows Class and Title for each top level Window
         Debug.Print "Top level Class = "; winClass; ", Title = "; winTitle
         ' Usually either enumerate Child or Thread Windows, not both.
         ' In this example, EnumThreadWindows may produce a very long list!
         Retval = EnumChildWindows(lhWnd, AddressOf EnumChildProc, lParam)
         ThreadID = GetWindowThreadProcessId(lhWnd, ProcessID)
         Retval = EnumThreadWindows(ThreadID, AddressOf EnumThreadProc, _
         lParam)
         EnumWinProc = True
      End Function

      Public Function EnumChildProc(ByVal lhWnd As Long, ByVal lParam As Long) _
         As Long
         Dim Retval As Long
         Dim WinClassBuf As String * 255, WinTitleBuf As String * 255
         Dim winClass As String, winTitle As String
         Dim WinRect As RECT
         Dim WinWidth As Long, WinHeight As Long
         Dim index2(200) As String
         Dim i As Long
         Retval = GetClassName(lhWnd, WinClassBuf, 255)
         winClass = StripNulls(WinClassBuf)  ' remove extra Nulls & spaces
         Retval = GetWindowText(lhWnd, WinTitleBuf, 255)
         winTitle = StripNulls(WinTitleBuf)
         ChildCount = ChildCount + 1
         
         i = i + 1
         ' see the Windows Class and Title for each Child Window enumerated
         index2(i) = winTitle
          Debug.Print " Child Class = " & winClass & ", Title = " & index2(i)
         
         ' You can find any type of Window by searching for its WinClass
         If winClass = "ThunderTextBox" Then    ' TextBox Window
            Retval = GetWindowRect(lhWnd, WinRect)  ' get current size
            WinWidth = WinRect.Right - WinRect.Left ' keep current width
            WinHeight = (WinRect.Bottom - WinRect.Top) * 2 ' double height
            Retval = MoveWindow(lhWnd, 0, 0, WinWidth, WinHeight, True)
            EnumChildProc = False
         Else
            EnumChildProc = True
         End If
      End Function
      

      Public Function EnumThreadProc(ByVal lhWnd As Long, ByVal lParam As Long) _
         As Long
         Dim Retval As Long
         Dim WinClassBuf As String * 255, WinTitleBuf As String * 255
         Dim winClass As String, winTitle As String

         Retval = GetClassName(lhWnd, WinClassBuf, 255)
         winClass = StripNulls(WinClassBuf)  ' remove extra Nulls & spaces
         Retval = GetWindowText(lhWnd, WinTitleBuf, 255)
         winTitle = StripNulls(WinTitleBuf)
         ThreadCount = ThreadCount + 1
         ' see the Windows Class and Title for top level Window
         Debug.Print "Thread Window Class = "; winClass; ", Title = "; _
         winTitle
         EnumThreadProc = True
      End Function

      Public Function StripNulls(OriginalStr As String) As String
         ' This removes the extra Nulls so String comparisons will work
         If (InStr(OriginalStr, Chr(0)) > 0) Then
            OriginalStr = Left(OriginalStr, InStr(OriginalStr, Chr(0)) - 1)
         End If
         StripNulls = OriginalStr
      End Function



Public Function GetMenuText(ByVal hMenu As Long, ByVal wIDItem As Long) As String
    Dim C As Long
    Dim S As String
    ' Get the text size
    C = GetMenuString(hMenu, wIDItem, vbNullString, 0, 0)
    ' Allocate the string
    S = String(C + 1, 0)
    ' Get the text
    C = GetMenuString(hMenu, wIDItem, S, C + 1, 0)
    GetMenuText = S
End Function

Public Function GetObjectText(hWnd As Long) As String
Dim i As Long
Dim S As String

i = SendMessage(hWnd, WM_GETTEXTLENGTH, ByVal 0&, ByVal 0&)

S = Space$(i + 1)
SendMessage hWnd, WM_GETTEXT, ByVal i + 1, ByVal S

GetObjectText = CleanString(S)

End Function

Function ToggleCapsLock(varCase As Long)
    Dim Res As Long
    Dim KBState(0 To 255) As Byte
    Res = GetKeyboardState(KBState(0))
    KBState(&H14) = varCase
    Res = SetKeyboardState(KBState(0))
End Function

Function CapsLockStatus() As Boolean
    Dim Res As Long
    Dim KBState(0 To 255) As Byte
        Sleep 250
    Res = GetKeyboardState(KBState(0))
        Sleep 250
    CapsLockStatus = KBState(&H14) And 1
        Sleep 250
End Function


Public Function CapsLock() As Boolean
    Sleep 250
    CapsLock = KeyState(kCapital)
End Function

Public Function NumLock() As Boolean
    NumLock = KeyState(kNumlock)
End Function

Private Function KeyState(lKey As Long) As Boolean
    Sleep 250
    KeyState = CBool(GetKeyState(lKey))
End Function

Function ProcIDFromWnd(ByVal hWnd As Long) As Long
   Dim idProc As Long

   ' Get PID for this HWnd
   GetWindowThreadProcessId hWnd, idProc
   ProcIDFromWnd = idProc
End Function

Function GetWinHandle(hInstance As Long) As Long
   Dim tempHwnd As Long
     
   ' Grab the first window handle that Windows finds:
   tempHwnd = FindWindow(vbNullString, vbNullString)
  
   ' Loop until you find a match or there are no more window handles:
   Do Until tempHwnd = 0
      ' Check if no parent for this window
      If GetParent(tempHwnd) = 0 Then
         ' Check for PID match
         If hInstance = ProcIDFromWnd(tempHwnd) Then
            ' Return found handle
            GetWinHandle = tempHwnd
            ' Exit search loop
            Exit Do
         End If
      End If
  
      ' Get the next window handle
      tempHwnd = GetWindow(tempHwnd, GW_HWNDNEXT)
   Loop
End Function
