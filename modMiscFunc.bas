Attribute VB_Name = "modMiscFunc"
Public Target_Path As String
Public Data As String
Public TimeStart As String
Public TimeFinish As String

Public Sub KillExistingCitrixSession()
    Dim Action As Long
    On Error Resume Next
    ActiveWorkbook.Save
    Application.DisplayAlerts = False
     '//shut down the computer
    Action = ExitWindowsEx(EWX_SHUTDOWN, 0&)
    Application.Quit
End Sub



Public Function onlyDigits(S As String) As String
    ' Variables needed (remember to use "option explicit").   '
    Dim Retval As String    ' This is the return string.      '
    Dim i As Integer        ' Counter for character position. '

    ' Initialise return string to empty                       '
    Retval = ""

    ' For every character in input string, copy digits to     '
    '   return string.                                        '
    For i = 1 To Len(S)
        If Mid(S, i, 1) >= "0" And Mid(S, i, 1) <= "9" Then
            Retval = Retval + Mid(S, i, 1)
        End If
    Next

    ' Then return the return string.                          '
    onlyDigits = Retval
End Function

Function CharacterArray(Value As String) As String
    With CreateObject("vbscript.regexp")
        .Pattern = "(.)"
        .Global = True
        adddots = .Replace(Value, "$1.")
        adddots = Left(adddots, Len(adddots) - 1)
    End With
    CharacterArray = adddots
End Function

Function CleanString(StrIn As String) As String
    ' "Cleans" a string by removing embedded control (non-printable)
    ' characters, including carriage returns and linefeeds.
    ' Does not remove special characters like symbols, international
    ' characters, etc. This function runs recursively, each call
    ' removing one embedded character
    Dim iCh As Integer
    CleanString = StrIn
    For iCh = 1 To Len(StrIn)
        If Asc(Mid(StrIn, iCh, 1)) < 32 Then
            'remove special character
            CleanString = Left(StrIn, iCh - 1) & CleanString(Mid(StrIn, iCh + 1))
            Exit Function
        End If
    Next iCh
End Function

Sub MacroGetCFSLNIReport()
    On Error Resume Next
    Dim testWorkbook As Workbook, asid As String, PreCheckVar As Boolean, IfNeedSRI As Boolean, LastRow As Integer, _
        i As Integer, CFSTES As Long
    Dim PreviousDate As String, TodayDate As String, DateArr() As String, DayValue As String, MonthValue As String, YearValue As String
'    Data = ""
'    Do While Data = ""
'        Open "V:\Chorus Shared\Chorus\Citrix Transfer Folder\# BBIP Script Stuff\Jono Tool Tools\Sv\CONFIGURATION_FILES\SETTINGS\cfs_report_location.txt" For Input As #1
'        Line Input #1, Data
'    Loop
    Data = "V:\Chorus Shared\Chorus\Citrix Transfer Folder\# BBIP Script Stuff\Jono Tool Tools\Sv\CFS\CFS_2\"
    PreviousDate = Format(Now() - i, "dd/MM/YYYY")
    DateArr = Split(PreviousDate, "/")
    DayValue = DateArr(0)
    MonthValue = DateArr(1)
    YearValue = DateArr(2)
    fileName = DayValue & MonthValue & Right(YearValue, 2)
    Target_Path = Data & fileName & ".csv"
    PreCheckVar = PreCheckLNIReport
    If PreCheckVar = True Then
        Set testWorkbook = InitializingLNIReport
        LastRow = DATASHEET.Range("A" & Rows.Count).End(xlUp).row
        For i = 2 To LastRow
            CFSTES = 0
            asid = DATASHEET.Range("E" & i).Value
            DATASHEET.Range("F" & i).Value = getCFSfromLNIReport(asid, testWorkbook)
            CFSTES = CLng(Right(DATASHEET.Range("F" & i).Value, 7))
            If (InStr(DATASHEET.Range("B" & i).Value, "Disconnect") > 0) Or _
            DATASHEET.Range("B" & i).Value = "Modify Attribute" Then
                DATASHEET.Range("F" & i).Value = "CFS NOT REQUIRED"
            End If
'            If DATASHEET.Range("F" & i).Value = "ASID not found in CFS report" Then
'                IfNeedSRI = True
'                DATASHEET.Range("A" & i & ":AB" & i).Interior.Color = RGB(255, 0, 0)
'                'EnableSRIextraction = True
'            End If
'            If InStr(DATASHEET.Range("F" & i).Value, "CFS") > 0 Then
'                If InStr(DATASHEET.Range("AA" & i).Value, "TESV1") > 0 Then Sheet1.Range("F" & i).Value = DATASHEET.Range("F" & i).Value & vbNewLine & "CFSV1" & CFSTES
'                If InStr(DATASHEET.Range("AA" & i).Value, "TESB1") > 0 Then Sheet1.Range("F" & i).Value = DATASHEET.Range("F" & i).Value & vbNewLine & "CFSB1" & CFSTES
'            End If
            Rows("1:1").EntireRow.AutoFit
        Next
        closingLNIReport testWorkbook
        PreCheckVar = False
    Else
        MsgBox "Unable to Locate Ltest CFS Report."
    End If
End Sub

Function PreCheckLNIReport() As Boolean
    On Error Resume Next
    Dim fileName As String
    Dim PreviousDate As String, TodayDate As String, DateArr() As String, DayValue As String, MonthValue As String, YearValue As String, _
        i As Integer
    Dim pTarget_Path As String
    
    For i = 1 To 5
        PreviousDate = Format(Now() - i, "dd/MM/YYYY")
        DateArr = Split(PreviousDate, "/")
        DayValue = DateArr(0)
        MonthValue = DateArr(1)
        YearValue = DateArr(2)
        fileName = DayValue & MonthValue & Right(YearValue, 2)
        pTarget_Path = Data & fileName & ".csv"
        If Dir(pTarget_Path) <> "" Then
            PreCheckLNIReport = True
        Else
            PreCheckLNIReport = False
        End If
        While PreCheckLNIReport = True
            Kill pTarget_Path
            PreCheckLNIReport = False
        Wend
    Next
    TodayDate = Format(Now(), "dd/MM/YYYY")
    DateArr = Split(TodayDate, "/")
    DayValue = DateArr(0)
    MonthValue = DateArr(1)
    YearValue = DateArr(2)
    fileName = DayValue & MonthValue & Right(YearValue, 2)
    If Dir(Target_Path) <> "" Then
        PreCheckLNIReport = True
    Else
        PreCheckLNIReport = False
    End If
End Function

Function InitializingLNIReport() As Workbook
    On Error Resume Next
    Dim Target_Workbook As Workbook
    Dim fileName As String
    Dim TodayDate As String, DateArr() As String, DayValue As String, MonthValue As String, YearValue As String
    
    TodayDate = Format(Now(), "dd/MM/YYYY")
    DateArr = Split(TodayDate, "/")
    DayValue = DateArr(0)
    MonthValue = DateArr(1)
    YearValue = DateArr(2)
    fileName = DayValue & MonthValue & Right(YearValue, 2)
    Set Target_Workbook = Workbooks.Open(fileName:=Target_Path, ReadOnly:=True)
    Target_Workbook.Application.WindowState = xlMinimized
    Set InitializingLNIReport = Target_Workbook
End Function

Function getCFSfromLNIReport(ByVal asid As String, Target_Workbook As Workbook) As String
    Dim Rng As Object
    On Error Resume Next
    'Target_Workbook.Application.WindowState = xlNormal
    Target_Workbook.Activate
    Target_Workbook.ActiveSheet.Columns("B:B").Select
    Target_Workbook.Activate
    'Target_Workbook.Application.WindowState = xlNormal
    Set Rng = Selection.Find(what:=asid, After:=Target_Workbook.ActiveSheet.Range("B1"), _
        LookIn:=xlFormulas, lookat:=xlWhole, SearchOrder:=xlByRows, _
        SearchDirection:=xlNext, _
        MatchCase:=False, SearchFormat:=False)
    If Rng Is Nothing Then
        getCFSfromLNIReport = "ASID not found in CFS report"
    Else
        getCFSfromLNIReport = Rng.offset(0, -1).Value
    End If
End Function

Sub closingLNIReport(ByVal Target_Workbook As Workbook)
    On Error Resume Next
    Target_Workbook.Close False
End Sub

Function RemoveDupesColl(MyArray As Variant) As Variant
    Dim i As Long
    Dim arrColl As New Collection
    Dim arrDummy() As Variant
    Dim arrDummy1() As Variant
    Dim item As Variant
    ReDim arrDummy1(LBound(MyArray) To UBound(MyArray))

    For i = LBound(MyArray) To UBound(MyArray) 'convert to string
        arrDummy1(i) = CStr(MyArray(i))
    Next i
    On Error Resume Next
    For Each item In arrDummy1
       arrColl.Add item, item
    Next item
    Err.Clear
    ReDim arrDummy(LBound(MyArray) To arrColl.Count - LBound(MyArray) - 1)
    i = LBound(MyArray)
    For Each item In arrColl
       arrDummy(i) = item
       i = i + 1
    Next item
    RemoveDupesColl = arrDummy
End Function

Function IsInArray(valToBeFound As Variant, arr As Variant) As Boolean
'Function Owner: Jeff Zhang(Jeff.Zhang@Chorus.co.nz); Owned by Chorus NZ
'DESCRIPTION: Function to check if a value is in an array of values
'INPUT: Pass the function a value to search for and an array of values of any data type.
'OUTPUT: True if is in array, false otherwise
Dim element As Variant
On Error GoTo IsInArrayError: 'array is empty
    For Each element In arr
        If IsNumeric(element) = True Then element = CInt(element)
        If element = valToBeFound Then
            IsInArray = True
            Exit Function
        End If
    Next element
Exit Function
IsInArrayError:
On Error GoTo 0
IsInArray = False
End Function

Sub testefwesf()
ImportRSPListfromCSV "V:\Chorus Shared\Chorus\Citrix Transfer Folder\# BBIP Script Stuff\Jono Tool Tools\Sv\CONFIGURATION_FILES\BILLING_APP_COEXISTENCE_MANAGER\RSP_TO_BE_IGNORED_IN_SV.csv"
Stop
End Sub

Sub ImportRSPListfromCSV(ByVal PathandFileName As String)
    Dim fileObject As Object, fileName As String, RSP_Picker As String, getfile As String
    Set fs = CreateObject("Scripting.FileSystemObject")
    NGA_PARAMETERS.Activate
    NGA_PARAMETERS.Range("AG1").Value = "MIGRATED_RSP_LIST"
    NGA_PARAMETERS.Range("AG:AH").ClearContents
    NGA_PARAMETERS.Range("AG1").Value = "RSP_ACCOUNT"
    NGA_PARAMETERS.Range("AH1").Value = "MIGRATION_DATE"
    NGA_PARAMETERS.Range("AG1").Select
    NextExeptionsRow = NGA_PARAMETERS.Range("AG" & Rows.Count).End(xlUp).row + 1
    NextExeptionsRow1 = NextExeptionsRow
    NextExeptionsRow = "$AG$" & NextExeptionsRow
    On Error Resume Next
    getfile = PathandFileName
    With NGA_PARAMETERS.QueryTables.Add(Connection:= _
        "TEXT;" & getfile, Destination:=Range( _
        NextExeptionsRow))
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = True
        .RefreshPeriod = 0
        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 437
        .TextFileStartRow = 2
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        .TextFileColumnDataTypes = Array(1, 1, 1, 1, 1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        ThisWorkbook.Connections("RSP_TO_BE_IGNORED_IN_SV").Delete
    End With
NewLastExeptionsRow = NGA_PARAMETERS.Range("AG" & Rows.Count).End(xlUp).row
NGA_PARAMETERS.Activate
With NGA_PARAMETERS
    Columns("A:G").Select
    Columns("A:G").EntireColumn.AutoFit
    .Range("A1").Select
End With
NGA_PARAMETERS.Activate
End Sub

