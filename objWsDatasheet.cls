VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "objWsDatasheet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private ws As Worksheet
Private aConnSession As Boolean
Private aPortalID As Long
Private aType As String
Private aAcctID As Long
Private aStartDate As String
Private aasid As Long
Private aOldAsid As String
Private aSAMID As Long
Private aCWOOrder As Long
Private aS8Order As Long
Private aX2Order As Long
Private aOrderTp As Long
Private aOrderStatus  As String
Private aSOReq As String
Private aMemo As String
Private aNotes As String
Private aErrCode As String
Private aColPortalID As String
Private aColType As String
Private aColAcctNo As String
Private aColStartDate As String
Private aColAsid As String
Private aColOldAsid As String
Private aColSAM As String
Private aColCWOOrder As String
Private aColS8Order As String
Private aColX2Order As String
Private aColOType As String
Private aColStatus As String
Private aColReq As String
Private aColMemo As String
Private aColNotes As String
Private aColErrCode As String
Private aRowNo As Long


Property Let portalID(portalID As Long)
    aPortalID = portalID
    ws.Range(aColPortalID & aRowNo).Value = portalID
End Property

Property Let orderType(orderType As String)
    If InStr(orderType, ":") > 0 Then
        aType = Trim(Split(orderType, ":")(1))
    End If
    ws.Range(aColType & aRowNo).Value = aType
End Property

Property Let AcctID(AcctID As Long)
    aAcctID = AcctID
    ws.Range(aColAcctNo & aRowNo).Value = AcctID
End Property

Property Let StartDate(StartDate1 As String)
    Dim startdate_Year As String, startdate_Month As String, startdate_Day As String, StartDate() As String
    If InStr(StartDate1, ":") > 0 Then
        aStartDate = Trim(Split(StartDate1, ":")(1))
        StartDate = Split(aStartDate, ",")
        startdate_Year = Left(Trim(StartDate(2)), 4)
        StartDate = Split(StartDate(1), " ")
        startdate_Day = Format(onlyDigits(StartDate(1)), "00")
        startdate_Month = Format(month(startdate_Day & "-" & StartDate(2) & "-" & startdate_Year), "00")
        aStartDate = CStr(startdate_Day & "/" & startdate_Month & "/" & startdate_Year)
        startdate_Year = vbNullString
        startdate_Month = vbNullString
        startdate_Day = vbNullString
    End If
    ws.Range(aColStartDate & aRowNo).Value = aStartDate
End Property

Property Let asid(asid As Long)
    aasid = asid
    ws.Range(aColAsid & aRowNo).Value = aasid
End Property

Property Let OldASID(OldASID As Long)
    aOldAsid = OldASID
    ws.Range(aColOldAsid & aRowNo).Value = OldASID
End Property

Property Let SAMID(SAMID As String)
    aSAMID = SAMID
    ws.Range(aColSAM & aRowNo).Value = SAMID
End Property

Property Let CWOOrder(Order As Long)
    aCWOOrder = Order
    ws.Range(aColCWOOrder & aRowNo).Value = Order
End Property

Property Let S8Order(Order As Long)
    aS8Order = Order
    ws.Range(aColS8Order & aRowNo).Value = Order
End Property

Property Let X2Order(Order As Long)
    aX2Order = Order
    ws.Range(aColX2Order & aRowNo).Value = Order
End Property

Property Let OrderStatus(OrderStatus As String)
    aOrderStatus = OrderStatus
    ws.Range(aColStatus & aRowNo).Value = OrderStatus
End Property

Property Let soReq(soReq As String)
    aSOReq = soReq
    ws.Range(aColReq & aRowNo).Value = CStr(soReq)
End Property

Property Let memo(memo As String)
    aMemo = memo
    ws.Range(aColMemo & aRowNo).Value = memo
End Property

Property Let Notes(Notes As String)
    aNotes = Notes
    ws.Range(aColNotes & aRowNo).Value = Notes
End Property

Property Let ErrCode(ErrCode As String)
    If ws.Range(aColErrCode & aRowNo).Value = "" Then
        ws.Range(aColErrCode & aRowNo).Value = ErrCode
    Else
        ws.Range(aColErrCode & aRowNo).Value = ws.Range(aColErrCode & aRowNo).Value & vbNewLine & ErrCode
    End If
    aErrCode = ws.Range(aColErrCode & aRowNo).Value
End Property

Public Function InitSession(ByVal row As Long) As Boolean
    aRowNo = row
    If row <> 0 Then
        aConnSession = True
        InitSession = True
        aPortalID = ws.Range(aColPortalID & aRowNo).Value
        aType = ws.Range(aColType & aRowNo).Value
        aAcctID = ws.Range(aColAcctNo & aRowNo).Value
        aStartDate = ws.Range(aColStartDate & aRowNo).Value
        aSAMID = ws.Range(aColSAM & aRowNo).Value
        aCWOOrder = ws.Range(aColCWOOrder & aRowNo).Value
        aasid = ws.Range(aColSAM & aRowNo).Value
        aOldAsid = ws.Range(aColOldAsid & aRowNo).Value
        aS8Order = ws.Range(aColS8Order & aRowNo).Value
        aX2Order = ws.Range(aColX2Order & aRowNo).Value
        aOrderStatus = ws.Range(aColStatus & aRowNo).Value
        aSOReq = ws.Range(aColReq & aRowNo).Value
        aNotes = ws.Range(aColNotes & aRowNo).Value
        aErrCode = ws.Range(aColErrCode & aRowNo).Value
    End If
End Function

Property Get getPortalID() As Long
    If IsNumeric(ws.Range(aColPortalID & aRowNo).Value) = True Then
        getPortalID = ws.Range(aColPortalID & aRowNo).Value
        aPortalID = getPortalID
    End If
End Property

Property Get getType() As String
    getType = ws.Range(aColType & aRowNo).Value
End Property

Property Get getAcctNo() As String
    getAcctNo = ws.Range(aColAcctNo & aRowNo).Value
End Property

Property Get getStartDate() As String
    getStartDate = ws.Range(aColStartDate & aRowNo).Value
End Property

Property Get getAsid() As Long
    If IsNumeric(ws.Range(aColAsid & aRowNo).Value) = True Then
        getAsid = ws.Range(aColAsid & aRowNo).Value
    End If
End Property

Property Get getOldAsid() As Long
    If IsNumeric(ws.Range(aColOldAsid & aRowNo).Value) = True Then
        getOldAsid = ws.Range(aColOldAsid & aRowNo).Value
    End If
End Property

Property Get getSAM() As Long
    If IsNumeric(ws.Range(aColSAM & aRowNo).Value) = True Then
        getSAM = ws.Range(aColSAM & aRowNo).Value
    End If
End Property

Property Get getCWOOrder() As Long
    getCWOOrder = ws.Range(aColCWOOrder & aRowNo).Value
End Property

Property Get getS8Order() As Long
    getS8Order = ws.Range(aColS8Order & aRowNo).Value
End Property

Property Get getX2Order() As Long
    getX2Order = ws.Range(aColX2Order & aRowNo).Value
End Property

Property Get getStatus() As String
    getStatus = ws.Range(aColStatus & aRowNo).Value
End Property

Property Get getOType() As String
    getOType = ws.Range(aColType & aRowNo).Value
    If getOType = "" Then
        getOType = aType
    End If
End Property

Property Get getReq() As String
    getReq = ws.Range(aColReq & aRowNo).Value
End Property

Property Get getMEMO() As String
    getMEMO = ws.Range(aColMemo & aRowNo).Value
End Property

Property Get getNotes() As String
    getNotes = ws.Range(aColNotes & aRowNo).Value
End Property

Property Get getErrCode() As String
    getErrCode = ws.Range(aColErrCode & aRowNo).Value
End Property

Public Function UpdateDatasheet() As Boolean
    If aRowNo <> 0 Then
        UpdateDatasheet = True
    Else
        UpdateDatasheet = False
    End If
End Function

Public Function lstRow() As Long
    lstRow = ws.Range(aColPortalID & Rows.Count).End(xlUp).row
End Function

Public Sub highLightRow(ColorValue As Long)
    ws.Range("A" & aRowNo & ":N" & aRowNo).Interior.Color = ColorValue
End Sub

Public Sub resetRowCol()
    ws.Range("A" & aRowNo & ":N" & aRowNo).Interior.Pattern = xlNone
End Sub

Public Function getRowColIndx() As Long
    getRowColIndx = ws.Range("A" & aRowNo & ":N" & aRowNo).Interior.ColorIndex
End Function

Public Sub resetRowHeight()
    ws.Rows(aRowNo).RowHeight = 15
End Sub

Private Sub Class_Initialize()
    Dim ini As New clsINILoader, section As String
    section = "DATASHEET_COLUMN_REF"
    If ini.iniFileLocation(modConstValues.config_file) = True Then
        With ini
            Set ws = Sheets(.getReturnValue("WORKBOOK_PARAMS", "DATASHEET_NAME"))
            aColPortalID = .getReturnValue(section, "COL_PORTAL_ID")
            aColType = .getReturnValue(section, "COL_TYPE")
            aColAcctNo = .getReturnValue(section, "COL_ACCT_NO")
            aColStartDate = .getReturnValue(section, "COL_START_DATE")
            aColAsid = .getReturnValue(section, "COL_ASID")
            aColOldAsid = .getReturnValue(section, "COL_OLD_ASID")
            aColSAM = .getReturnValue(section, "COL_SAM")
            aColCWOOrder = .getReturnValue(section, "COL_CWO_ORDER")
            aColS8Order = .getReturnValue(section, "COL_S8_ORDER")
            aColX2Order = .getReturnValue(section, "COL_X2_ORDER")
            aColStatus = .getReturnValue(section, "COL_ICMS_STATUS")
            aColReq = .getReturnValue(section, "COL_SO_REQ")
            aColMemo = .getReturnValue(section, "COL_MEMO")
            aColNotes = .getReturnValue(section, "COL_NOTES")
            aColErrCode = .getReturnValue(section, "COL_ERROR_CODE")
            ws.Columns(aColStartDate & ":" & aColStartDate).NumberFormat = "@"
        End With 'ini
    End If
    Set ini = Nothing
End Sub
'ws.Columns(aColPortalID & ":" & aColErrCode).AutoFit
