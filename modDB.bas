Attribute VB_Name = "modDB"
Public sql_str_txd_folder As String
Public db_folder As String
Public sql_insert As String
Public sql_query As String


Function modDBInit() As Boolean
    Dim ini As New clsINILoader
    ini.iniFileLocation (modConstValues.config_file)
    modDB.sql_str_txd_folder = ini.getReturnValue("DB_PARAMS", "TXT_FILE_FOLDER")
    modDB.db_folder = ini.getReturnValue("DB_PARAMS", "DB_PATH")
    modDB.sql_query = ini.getReturnValue("DB_PARAMS", "SELECT_QRY")
End Function

Sub InsertData(ByVal rownumber As Long)
    Application.EnableCancelKey = xlDisabled
    Dim record_str As String, Person As String
    Dim SQL As String, SQL2 As String
    Dim error_code As String, billing_time As String, billing_date As String
    Dim ws As New objWsDatasheet
    Dim fso As FileSystemObject: Set fso = New FileSystemObject
    Dim oFile As Object
    Dim aFile As String
    Dim objNetwork As Object, winapi As New clsWinAPI
RetryInsert:

    Person = DASHBOARD.Range(portal_login_id_rg).Value
    Err.Clear
    billing_time = Format(Now(), "hh:mm")
    billing_date = Format(Now(), "dd/MM/YYYY")
    With ws
        .InitSession rownumber
        error_code = replace(.getErrCode,vbNewLine, "|")
        record_str = .getPortalID & "," & _
                    .getType & "," & _
                    .getAcctNo & "," & _
                    .getStartDate & "," & _
                    .getAsid & "," & _
                    .getOldAsid & "," & _
                    .getSAM & "," & _
                    .getCWOOrder & "," & _
                    .getS8Order & "," & _
                    .getX2Order & "," & _
                    .getStatus & "," & _
                    .getReq & "," & _
                    .getNotes & "," & _
                    .getErrCode & "," & _
                    billing_time & "," & _
                    billing_date & "," & _
                    Person
    End With 'ws
    aFile = sql_str_txd_folder & "\" & ws.getPortalID & ".txt"
    winapi.Sleeping 150
    Set oFile = fso.CreateTextFile(aFile)
    winapi.Sleeping 150
    oFile.WriteLine record_str
    oFile.Close
    Set oFile = Nothing
    Set fso = Nothing
    Set ws = Nothing
    Set winapi = Nothing
    Application.EnableCancelKey = xlInterrupt
End Sub

Function ifPortalIDExsit(ByVal rownumber As Long) As Boolean
    Application.EnableCancelKey = xlDisabled
    Dim strSql As String, strConnection As String
    Dim dbConnectStr As String
    Dim rs As Object
    Dim SQL As String
    Dim portalID As Long
    Dim i As Integer
    Dim fso As FileSystemObject: Set fso = New FileSystemObject
    Dim oFile As Object
    Dim aFile As String
    Dim objNetwork As Object
    Dim ws As New objWsDatasheet
    Set objNetwork = VBA.CreateObject("WScript.Network")
    With ws
        .InitSession rownumber
        portalID = .getPortalID
    End With
    aFile = sql_str_txd_folder & "\" & ws.getPortalID & ".txt"
    ifPortalIDExsit = fso.FileExists(aFile)
    Set oFile = Nothing
    Set fso = Nothing
    Set objNetwork = Nothing
    Set ws = Nothing
End Function
