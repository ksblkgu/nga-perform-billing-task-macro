Attribute VB_Name = "Procedure_ChorusPortal"
Option Compare Text

Public LoginID As String, CPPassword As String, npe As Boolean, portal_url As String

Public Sub portalProcess()
    Dim winapi As New clsWinAPI
    Dim wd As New objWsDatasheet, row As Long, lRow As Long, soReq As String
    Dim runICMSProcess As Boolean: runICMSProcess = False
    Dim chkAutoIcms As Integer
    
    ThisWorkbook.Application.WindowState = xlMinimized
    If DASHBOARD.Range(modConstValues.portal_login_id_rg).Value = "" Or DASHBOARD.Range(modConstValues.portal_login_pass_rg).Value = "" Then
        MsgBox "Invalid Portal Login Details"
        Exit Sub
    End If
    If DASHBOARD.Range(modConstValues.portal_login_id_rg).Value = "" Or DASHBOARD.Range(modConstValues.portal_login_pass_rg).Value = "" Then
        MsgBox "Invalid ICMS Login Details"
        Exit Sub
    End If
    If DASHBOARD.chkRESET_DATASHEET.Value = True Then
        Call assigningJobs
        DATASHEET.Range("A2:Z9999").Delete
    End If
    Call PullDetailsfromPortal
    DASHBOARD.Activate
    ThisWorkbook.Application.WindowState = xlNormal
    On Error Resume Next
    Err.Number = 1
    Do While Err.Number <> 0
        Err.Clear
        chkAutoIcms = DASHBOARD.Shapes("Check Box 10").OLEFormat.Object.Value
    Loop
    ThisWorkbook.Application.WindowState = xlMinimized
    On Error GoTo 0
    If chkAutoIcms = 1 Then
        winapi.Sleeping 2000
        lRow = wd.lstRow
        For row = 2 To lRow
            wd.InitSession row
            If wd.getReq <> "False" Then
                runICMSProcess = True
            End If
        Next
        If runICMSProcess = True Then Call CreateICMSOrder
    Else
        ThisWorkbook.Application.WindowState = xlNormal
        MsgBox "Assign&Pull from Portal Process has been completed"
    End If
End Sub


Private Sub assigningJobs()
    Dim ie_portal As InternetExplorer
    Dim portal_login_id As String: portal_login_id = DASHBOARD.Range(modConstValues.portal_login_id_rg).Value
    Dim portal_login_pass As String: portal_login_pass = DASHBOARD.Range(modConstValues.portal_login_pass_rg).Value
    Dim x As Integer: x = 1
    Dim page_param As Integer: page_param = DASHBOARD.Range(modConstValues.pg_param).Value
    ChorusPortalController.kill_exisiting_ie
    modMacroStatusIE.init_StatusIE
    status_update , "NGA Perform Billing Macro", "Auto Assigning Jobs"
    Set ie_portal = New InternetExplorer: itinalIEObj ie_portal
    ChorusPortalController.Login ie_portal, portal_login_id, portal_login_pass
    ChorusPortalController.BrowseCP ie_portal, "Work"
    ChorusPortalController.WorkQueuePageManager ie_portal, "NGA Provisioning", "Unassigned Tasks", _
                                                "All", "All", "All", "All", "All", "All", "All", _
                                                "Perform Billing", "All", True
    If page_param = 0 Then page_param = 99999
    Do While get_search_results_count(ie_portal) > 0
        If x > page_param Then Exit Do
        If ChorusPortalController.AssignJobstoOther(ie_portal, "ME") = False Then
            'Need log this error
            DoEvents
        End If
        x = x + 1
    Loop 'get_search_results_count(ie_portal) > 0
    Set ie_portal = Nothing
    DASHBOARD.chkJobList.Value = True
End Sub

Sub PullDetailsfromPortal()
    Dim CPLoginID As String, CPLoginPass As String, Maintenance As String
    Dim ie_summary As InternetExplorer, ie_network As InternetExplorer
    Dim ie_workorder As InternetExplorer, ie_charges As InternetExplorer
    Dim ie_complete As InternetExplorer
    Dim icms_order As Long
    Dim ws As objWsDatasheet
    Dim lstRow As Long, row As Long, x As Integer
    Dim memoList As Collection
    Dim asidHolder(2) As String, StraightPost As Boolean
    Dim winapi As New clsWinAPI
    
    LoginID = DASHBOARD.Range(modConstValues.portal_login_id_rg).Value
    CPPassword = DASHBOARD.Range(modConstValues.portal_login_pass_rg).Value
    If LoginID = "" Or CPPassword = "" Then
        MsgBox "Invalid Portal Login Details"
        Exit Sub
    End If
    ChorusPortalController.kill_exisiting_ie
    modMacroStatusIE.init_StatusIE
    status_update , "NGA Perform Billing Macro", "Pulling Order details from Portal"
    If LCase(DASHBOARD.Range("C36").Value) = "ppe" Then npe = True
    If npe = True Then
        portal_url = "https://portal.npe.chorus.co.nz/"
    Else
        portal_url = "https://portal.chorus.co.nz/chorus-ssp-web/"
    End If
    On Error Resume Next: Err.Number = 1
    Do While Err.Number <> 0
        Set ie_summary = New InternetExplorer: ie_summary.Toolbar = False: ie_summary.Visible = modConstValues.ie_visible
        Err.Clear
        Sleep 1000
    Loop
    Set ie_network = New InternetExplorer: ie_network.Toolbar = False: ie_network.Visible = modConstValues.ie_visible
    Set ie_workorder = New InternetExplorer: ie_workorder.Toolbar = False: ie_workorder.Visible = modConstValues.ie_visible
    Set ie_charges = New InternetExplorer: ie_charges.Toolbar = False: ie_charges.Visible = modConstValues.ie_visible
    Set ie_complete = New InternetExplorer: ie_complete.Toolbar = False: ie_complete.Visible = modConstValues.ie_visible
    itinalIEObj ie_summary
    Login ie_summary, LoginID, CPPassword
    Maintenance = ie_summary.Document.title
    If InStr(ie_summary.Document.title, "Maintenance") > 0 Then
        MsgBox "Portal Under Maintenance or Browser not auto-opened properly; Process terminated;Please wait for 1 min and try again"
        Exit Sub
    End If
    If ie_summary Is Nothing Then
        MsgBox "There`s an error occured while opening chorus portal; please wait for 1-2 mins and try it again"
        Exit Sub
    End If
    If DASHBOARD.chkJobList.Value = True Then
        DATASHEET.Activate
        DATASHEET.Range("A2:AB9999").Delete
        DATASHEET.Range("A1").Select
        status_update , "NGA Perform Billing Macro", "Pulling allocated job list from Portal"
        get_allocated_jobs ie_summary
    End If
    ChorusPortalController.BrowseCP ie_summary, "Search"
    Set ws = New objWsDatasheet: lstRow = ws.lstRow: Set ws = Nothing
    modDB.modDBInit
    x = 0
    For row = 2 To lstRow
        Set ws = New objWsDatasheet
        ie_network.Visible = modConstValues.ie_visible
        ie_workorder.Visible = modConstValues.ie_visible
        ie_charges.Visible = modConstValues.ie_visible
        ie_complete.Visible = modConstValues.ie_visible
        StraightPost = False
        ie_complete.Navigate "https://portal.chorus.co.nz/chorus-ssp-web/pages/OrderManagement/Search"
        If ws.InitSession(row) = True Then
            status_update , "NGA Perform Billing Macro", "Pulling Order details from Portal", "Processing row#" & row & " / " & lstRow
            If ws.getPortalID = 0 Then Exit For
            If ws.getOType = "" Then
                ChorusPortalController.Tab_SearchOrders ie_summary, ws.getPortalID
                order_tab_click ie_summary, ie_workorder, "Work Order": ie_workorder.Visible = modConstValues.ie_visible
                order_tab_click ie_summary, ie_network, "Network Availability": ie_network.Visible = modConstValues.ie_visible
                order_tab_click ie_summary, ie_charges, "Charges": ie_charges.Visible = modConstValues.ie_visible
                ws.AcctID = get_acct(ie_charges) 'Get RSP Account Number from Charge Tab
                ws.orderType = Trim(summary_tab_object(ie_summary, "Order Type:").innerText) ' Order Type
                ws.StartDate = Trim(summary_tab_object(ie_summary, "Service Given Date:").innerText)  'Service Given Date
                asidHolder(1) = getNewAASID(ie_summary)
                ws.asid = asidHolder(1)
                If InStr(ws.getOType, "Transfer") > 0 Or InStr(ws.getOType, "Move") > 0 Then
                     asidHolder(2) = getOldAsid(ie_summary)
                     ws.OldASID = asidHolder(2)
                End If
                ws.SAMID = get_sam_id(ie_network)
                icms_order = ChorusPortalController.GetCSEOrderNo(ie_workorder)
                If icms_order = 0 Then
                    If ws.getOType = "Modify Attribute" Then
                        ws.soReq = "False"
                    ElseIf ws.getOType = "Change Offer" Then
                        ws.soReq = "False"
                    Else
                        ws.soReq = "True"
                    End If
                    If (Left(ws.getOType, 7) = "Connect") Then ws.soReq = "S8"
                    If (Left(ws.getOType, 8) = "Transfer") Or (Left(ws.getOType, 4) = "Move") Then ws.soReq = "S8&X2"
                    If (Left(ws.getOType, 10) = "Disconnect") Then ws.soReq = "X2"
                Else
                    ws.soReq = "False"
                    ws.CWOOrder = icms_order
                    ws.S8Order = 0
                    ws.X2Order = 0
                    ws.memo = "NA"
                    If (Left(ws.getOType, 8) = "Transfer") Then ws.soReq = "X2"
                    ws.OrderStatus = GetCSEOrderStatus(ie_workorder, icms_order)
                End If
                ws.memo = get_memo(ie_summary)
                ws.resetRowHeight
                If ws.getReq = "False" And icms_order = 0 Then
                    ws.OrderStatus = "Completed"
                    StraightPost = True
                End If
                If ws.getReq = "False" And ws.getCWOOrder > 0 And InStr("Completed_Billing", ws.getStatus) > 0 Then StraightPost = True
                If ws.getCWOOrder > 0 And ws.getStatus = "Pending" Then ws.highLightRow 65535
                If StraightPost = True Then
                    status_update , "NGA Perform Billing Macro", "CWO Order detected and status in completed", "Completing perform billing task" & vbNewLine & "row#" & row & " / " & lstRow
                    complete_perform_billing_task ie_complete, row
                    If modDB.ifPortalIDExsit(row) = False Then modDB.InsertData row
                End If
                ChorusPortalController.BrowseCP ie_summary, "Search"
            End If
            For x = 0 To 10
                Set ws = Nothing
                winapi.Sleeping 250
                If ws Is Nothing Then Exit For
            Next
        End If ' ws.orderType = ""
        x = x + 1
        If x = 6 Then x = 0
        On Error Resume Next
        If x = 5 Then ThisWorkbook.Save
        On Error GoTo 0
    Next
    Set winapi = Nothing
    ChorusPortalController.kill_exisiting_ie
    On Error Resume Next: ThisWorkbook.Save
End Sub



Sub complete_perform_billing_task(ByVal ie As InternetExplorer, rownumber As Long)
'This function cannot be called individually,
'It highly dependent on:    1. pre-defined ie object which has been initilized and navgivated to portal search page
'                           2. in arg: row which referent in Datasheet and valid portal id presents in column A
    Dim btnLogin As Object, btnSearch As Object
    Dim findvalue As Object
    Dim link As HTMLAnchorElement 'define variable as HTMLAnchorElement
    Dim ws As New objWsDatasheet: ws.InitSession rownumber
    
    If ie Is Nothing Then Exit Sub
    If rownumber < 1 Then Exit Sub
    With ie
        ie.Visible = modConstValues.ie_visible
        If InStr(.Document.URL, "/Search") < 1 Then
            BrowseCP ie, "Search": CPWait ie
        End If
        ChorusPortalController.Tab_SearchOrders ie, ws.getPortalID
    End With
    ChorusPortalController.order_tab_click ie, ie, "Tasks"
'Click "Perform billing" task
    For Each link In ie.Document.getElementsByTagName("a")
        If Trim(link.innerText) = "Perform Billing" Then
            ie.Visible = modConstValues.ie_visible
            link.Click: CPWait ie: Exit For
        End If
    Next
    On Error Resume Next: Err.Clear
    Set Notes = ie.Document.getElementsByName("note")
    If Not Notes Is Nothing Then
        Notes(0).Value = "Perform Billing Task completed by NGA Billing Macro " & DASHBOARD.Range("D35")
    End If
    i_summary = 1
    CPWait ie
    Set Completed = ie.Document.getElementsByName("validateForComplete")
    If InStr(Completed(0).innerText, "Complete") > 0 Then
        ie.Visible = modConstValues.ie_visible
        Completed(0).Click
        CPWait ie
        Set completed2 = ie.Document.getElementsByClassName("button continue float-right")
        If Not completed2 Is Nothing Then completed2(1).Click
        CPWait ie
        ws.highLightRow 5287936
        ws.Notes = "Completed"
    Else
        ws.highLightRow 6750105
        ws.Notes = "Perform Billing Task Already Completed"
    End If
    ie.Visible = modConstValues.ie_visible
    ChorusPortalController.BrowseCP ie, "Search"
    Set wsDS = Nothing
End Sub


Function fncSpPos(strPassedString As String, iNum As Integer) As Integer

fncSpPos = 0

While Len(strPassedString) And (iNum > 0)
    If InStr(1, strPassedString, " ") Then
        fncSpPos = fncSpPos + InStr(1, strPassedString, " ")
        strPassedString = Mid(strPassedString, InStr(1, strPassedString, " ") + 1)
        iNum = iNum - 1
    Else
        fncSpPos = 0
        Exit Function
    End If
Wend

If iNum Then fncSpPos = 0

End Function

Public Function CustomGT(separator As String, toParse As String) As String
    Dim d As New Scripting.Dictionary, part As Variant, i As Integer
    For Each part In Split(toParse, separator)
        d(part) = 1
    Next
    CustomGT = Join(d.Keys, Chr(10))
End Function
