VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsINILoader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Declare for reading INI files.
Private Declare Function GetPrivateProfileString Lib "kernel32" _
    Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
                                    ByVal lpKeyName As Any, _
                                    ByVal lpDefault As String, _
                                    ByVal lpReturnedString As String, _
                                    ByVal nSize As Long, _
                                    ByVal lpFileName As String) As Long
                                    
Private Declare Function WritePrivateProfileString Lib "kernel32" _
    Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
                                    ByVal lpKeyName As Any, _
                                    ByVal lpString As Any, _
                                    ByVal lpFileName As String) As Long

Private aIniFileLoc As String


Public Function iniFileLocation(ByVal Path As String) As Boolean
    If Len(Dir(Path)) = 0 Then
        iniFileLocation = False
    Else
        aIniFileLoc = Path
        iniFileLocation = True
    End If
End Function

Public Function getReturnValue(section As String, Key As String) As String
    Dim sRetBuf As String
    Dim iLenBuf As Long
    Dim sReturnValue As String
    If aIniFileLoc = "" Then getReturnValue = "Error"
    If getReturnValue <> "Error" Then
        sRetBuf = Space(255): iLenBuf = Len(sRetBuf)
        sReturnValue = GetPrivateProfileString(section, Key, "", sRetBuf, _
                        255, aIniFileLoc)
        getReturnValue = Trim(Left(sRetBuf, sReturnValue))
    End If
End Function

Public Function WriteIniFileString(ByVal Sect As String, ByVal Keyname As String, ByVal Wstr As String) As String
Dim Worked As Long
Dim iNoOfCharInIni As Variant
Dim sIniString As String

  iNoOfCharInIni = 0
  sIniString = ""
  If Sect = "" Or Keyname = "" Then
    MsgBox "Section Or Key To Write Not Specified !!!", vbExclamation, "INI"
  Else
    Worked = WritePrivateProfileString(Sect, Keyname, Wstr, aIniFileLoc)
    If Worked Then
      iNoOfCharInIni = Worked
      sIniString = Wstr
    End If
    WriteIniFileString = sIniString
  End If
End Function
