Attribute VB_Name = "Process_ICMSandPortalTask"
Option Compare Text
Option Explicit

Sub CreateICMSOrder()
    Dim LoginID As String, CPPassword As String
    Dim ws As New objWsDatasheet, winapi As New clsWinAPI
    Dim createS8 As caseOrderS8
    Dim createX2 As caseOrderX2
    Dim row As Long, lRow As Long, x As Integer
    lRow = ws.lstRow: Set ws = Nothing
    Dim tNo As String, objOI As New clsOrderInterface
    Dim ie As InternetExplorer
    
    ThisWorkbook.Application.WindowState = xlMinimized
    LoginID = DASHBOARD.Range(modConstValues.portal_login_id_rg).Value
    CPPassword = DASHBOARD.Range(modConstValues.portal_login_pass_rg).Value
    If LoginID = "" Or CPPassword = "" Then
        MsgBox "Invalid ICMS Login details"
        Exit Sub
    End If
    initialize_icms
    modDB.modDBInit
    tNo = objOI.getTNo: Set objOI = Nothing
    With ie
        ChorusPortalController.kill_exisiting_ie
        modMacroStatusIE.init_StatusIE
        status_update , "NGA Perform Billing Macro", "Initializing ICMS for creating work orders"
        Set ie = New InternetExplorer
        ChorusPortalController.itinalIEObj ie
        ChorusPortalController.Login ie, LoginID, CPPassword
        ChorusPortalController.BrowseCP ie, "Search"
    End With
    DATASHEET.Activate
    x = 0
    For row = 2 To lRow
        'Initializing Datasheet with in_arg: row
        ie.Visible = modConstValues.ie_visible
        Set ws = New objWsDatasheet
        ws.InitSession row
        status_update , "NGA Perform Billing Macro", "Analyzing job details to see if work orders are needed", "Processing row#" & row & " / " & lRow
        If ws.getRowColIndx = -4142 Or ws.getRowColIndx = 6 Then
            If ws.getPortalID = 0 Then Exit For
            'Check if X2 order is required
            If InStr(ws.getReq, "X2") > 0 Then
                If ws.getX2Order > 0 Then
                    ws.ErrCode = "ERROR_X2 ALREADY CREATED"
                Else
                    status_update , "NGA Perform Billing Macro", "Creating X2 order", "Processing row#" & row & " / " & lRow
                    Set createX2 = New caseOrderX2
                    With createX2
                        If ws.getX2Order = 0 Then
                            .AccountNo = ws.getAcctNo
                            .SAMID = ws.getSAM
                            If ws.getOldAsid <> 0 Then
                                .ExistingAsid = ws.getOldAsid
                            Else
                                .ExistingAsid = ws.getAsid
                            End If
                            .StartDate = ws.getStartDate
                            .StartTime = "AM"
                            .memo = ws.getMEMO
                            .tNo = tNo
                            .CreateOrderProcedure
                            ws.X2Order = .soNo
                            If .ErrCode <> "" Then ws.ErrCode = .ErrCode
                            If ws.getX2Order = 0 Then
                                ws.ErrCode = "ERROR_FAILED TO CREATE X2 ORDER"
                                ws.highLightRow 255
                            End If
                        End If
                    End With
                End If 'ws.getX2Order > 0
                ws.OrderStatus = "Completed"
            End If
            'Check if S8 order is required
            If InStr(ws.getReq, "S8") > 0 Then
                If ws.getS8Order > 0 Then
                    ws.ErrCode = "ERROR_S8 ALREADY CREATED"
                Else
                    status_update , "NGA Perform Billing Macro", "Creating S8 order", "Processing row#" & row & " / " & lRow
                    Set createS8 = New caseOrderS8
                    With createS8
                        If ws.getS8Order = 0 Then
                            .AccountNo = ws.getAcctNo
                            .SAMID = ws.getSAM
                            .ExistingAsid = ws.getAsid
                            .StartDate = ws.getStartDate
                            .StartTime = "AM"
                            .memo = ws.getMEMO
                            .tNo = tNo
                            .CreateOrderProcedure
                            ws.S8Order = .soNo
                            If .ErrCode <> "" Then ws.ErrCode = .ErrCode
                            If ws.getS8Order = 0 Then
                                ws.ErrCode = "ERROR_FAILED TO CREATE S8 ORDER"
                                ws.highLightRow 255
                            End If
                        End If
                    End With
                End If 'InStr(ws.getReq, "S8")
                ws.OrderStatus = "Completed"
            End If
            'Check CWO order status if there`s available one.
            If ws.getCWOOrder > 0 And ws.getStatus <> "Completed" Then
                status_update , "NGA Perform Billing Macro", "Checking CWO order status", "Processing row#" & row & " / " & lRow
                objOI.StagetoWWC
                objOI.StagetoOrderEnter
                objOI.EnterIntoExistingSO ws.getCWOOrder
                If InStr(objOI.getOrderStageInfo, "BILL") > 0 Then ws.OrderStatus = "Completed"
                If objOI.getOrderStatus = "POSTED" Then
                    ws.OrderStatus = "Completed"
                    ws.resetRowCol
                End If
                objOI.BacktoChorusMenu
            End If
            'Complete perform billing task in portal
            If ws.getStatus = "Completed" Then
                status_update , "NGA Perform Billing Macro", "Completing Perform Billing task in Portal", "Processing row#" & row & " / " & lRow
                Procedure_ChorusPortal.complete_perform_billing_task ie, row
            Else
                ws.ErrCode = "ERROR_OPEN SERVICE ORDER"
                ws.highLightRow 65535
            End If
            For x = 0 To 10
                Set ws = Nothing
                winapi.Sleeping 200
                If ws Is Nothing Then Exit For
            Next
            For x = 0 To 10
                Set createX2 = Nothing
                winapi.Sleeping 200
                If createX2 Is Nothing Then Exit For
            Next
            For x = 0 To 10
                Set createS8 = Nothing
                winapi.Sleeping 200
                If createS8 Is Nothing Then Exit For
            Next
            If modDB.ifPortalIDExsit(row) = False Then modDB.InsertData row
            x = x + 1
            If x = 6 Then x = 0
            On Error Resume Next
            If x = 5 Then ThisWorkbook.Save
        End If 'ws.getRowColIndx = 0
        On Error GoTo 0
    Next 'For row = 2 To lRow
    Set winapi = Nothing
    ThisWorkbook.Application.WindowState = xlNormal
    ChorusPortalController.kill_exisiting_ie
    DASHBOARD.Activate
    On Error Resume Next: ThisWorkbook.Save: On Error GoTo 0
    MsgBox "Process Completed"
End Sub

Sub run_orders(icms_order, order_type, rownumber)
    Dim SO As String, S8SO As String, X2SO As String
    initialize_icms
    If icms_order = 0 And Left(order_type, 7) = "Connect" Then
        DATASHEET.Range("Q" & rownumber) = "S8"
        SO = CreateS8Order(rownumber)
        DATASHEET.Range("P" & rownumber) = "Y"
        DATASHEET.Range("L" & rownumber) = SO
        DATASHEET.Range("M" & rownumber) = "POSTED"
    Else
        DATASHEET.Range("P" & rownumber) = "N"
    End If
    If icms_order = 0 And Left(order_type, 8) = "Transfer" Then
        DATASHEET.Range("Q" & rownumber) = "S8&X2"
        DATASHEET.Range("P" & rownumber) = "Y"
        S8SO = CreateS8Order(rownumber)
        X2SO = CreateX2Order(rownumber)
        DATASHEET.Range("L" & rownumber) = "S8#" & S8SO & "/X2#" & X2SO
        DATASHEET.Range("M" & rownumber) = "POSTED"
    ElseIf icms_order > 0 And DATASHEET.Range("B" & rownumber).Value Like "*Transfer*" Then
        DATASHEET.Range("Q" & rownumber) = "X2"
        DATASHEET.Range("P" & rownumber) = "Y"
        X2SO = CreateX2Order(rownumber)
        DATASHEET.Range("Q" & rownumber) = "X2#" & X2SO
        'DATASHEET.Range("M" & rownumber) = "POSTED"
    End If

    If Left(order_type, 10) = "Disconnect" Then
        SO = CreateX2Order(rownumber)
        DATASHEET.Range("Q" & rownumber) = "X2"
        DATASHEET.Range("P" & rownumber) = "Y"
        DATASHEET.Range("L" & rownumber) = SO
        DATASHEET.Range("M" & rownumber) = "POSTED"
    End If
    If icms_order = 0 And (order_type = "Change Offer" Or order_type = "Modify Attribute") Then
        DATASHEET.Range("M" & rownumber) = "POSTED"
    End If
End Sub

Function PrecheckBeforeRunOrders() As Boolean
    Dim icms As clsICMS
    Set icms = New clsICMS
    With icms
        If .ConnectPS <> True Then
            PrecheckBeforeRunOrders = False
        Else
            If .onChorusMainMenu <> True Then
                PrecheckBeforeRunOrders = False
            Else
                PrecheckBeforeRunOrders = True
            End If
        End If
    End With
End Function
Sub test()
    CreateS8Order 2
End Sub

Function CreateS8Order(ByVal rownumber As Integer) As String
    Dim Pass As Object, btnLogin As Object, btnSearch As Object, findvalue As Object, address As String
    Dim tnumber As String, LoginID As String, CPPassword As String
    Dim Account As Long, asid As Long, list(100) As String, i As Integer, i_summary As Integer, i_FieldEK As Integer, _
        SO As String, list_index As Integer, row_index As Integer, employee As String
    Dim RFS As String, tomorrow As String, Day As String, month As String, Year As String, arr() As String
    Dim portalID As Long, CPNumber As Long, OOTnumber As String, ServiceProviderRef As String, LocationID As String, _
        CustName As String, RSPName As String, JobStatus As String, SubStatus As String, SubmittedDate As String, _
        SubmittedPerson As String, RFSDate As String, ServiceGivenDate As String, ProductType As String
    Dim MemoTxt As String, MemoTxt_Arr() As String
    Dim Plan As String, SAMID  As String
    Dim icms As clsICMS
    
    tnumber = DASHBOARD.Range("Q21").Value
    Account = DATASHEET.Range("C" & rownumber).Value
    asid = DATASHEET.Range("E" & rownumber).Value
    tomorrow = Format(Date + 1, "dd/mm/yyyy")
    RFS = DATASHEET.Range("D" & rownumber).Value
    arr = Split(RFS, "/")
    Day = CStr(Trim(arr(0)))
    month = CStr(arr(1))
    Year = CStr(arr(2))
    MemoTxt = DATASHEET.Range("Y" & rownumber).Value
    MemoTxt_Arr = Split(MemoTxt, vbNewLine)
    SAMID = Trim(MemoTxt_Arr(UBound(MemoTxt_Arr)))
    Plan = DATASHEET.Range("G" & rownumber).Value
    Set icms = New clsICMS
    With icms
        If .ConnectPS = True Then '1. Check if ICMS Open
            If .onChorusMainMenu = True Then '2. Check if It`s on the Main Mnue
                .setCursor 24, 58
                .SendKeyString "01"
                .EnterKey
                If .onWWCMenu = True Then   '3.
                    For i = 1 To 2
                        .FunctionKey_05
                    Next
                    .setCursor 7, 44
                    .SendKeyString CStr(Account)
                    .setCursor 21, 19
                    .SendKeyString "04"
                    .EnterKey
                    If .onServiceOrderEntryScreenEnquiry = True Then '4
                        '.FunctionKey_12
                        .FunctionKey_06
                        '===Create S8 order Start===
                        .setCursor 3, 29
                        .SendKeyString "S8"
                        .setCursor 4, 29
                        .FieldExitKey
                        .setCursor 4, 29
                        .SendKeyString Right(tnumber, 6)
                        .EnterKey
                        .setCursor 5, 29
                        .SendKeyString "FICT"
                        .EnterKey
                        .setCursor 6, 28
                        .SendKeyString asid
                        .EnterKey
                        If .scrRead(24, 2, 78) Like "*already*" Then
                            .FunctionKey_03
                            .setCursor 4, 25
                            For i_FieldEK = 1 To 7
                                .FieldExitKey
                            Next
                            .setCursor 5, 25
                            .SendKeyString asid
                            .setCursor 10, 68
                            .SendKeyString "A"
                            .EnterKey
                            CreateS8Order = Trim(.scrRead(5, 13, 19))
                            .FunctionKey_03
                            .FunctionKey_12
                            .FunctionKey_12
                            Exit Function
                        End If
                        .setCursor 11, 35
                        .SendKeyString month
                        .setCursor 11, 73
                        .SendKeyString "AF"
                        '.setCursor 12, 73
                        '.FieldExitKey
                        .SendKeyString CStr(Day) & CStr(month) & Right(CStr(Year), 2) & "165901Y"
                        .FunctionKey_06
                        If CDate(RFS) < CDate(tomorrow) Then
                            .FunctionKey_14
                            .SendKeyString tnumber
                            .EnterKey
                        End If
                        For i = 1 To 15
                            .FieldExitKey
                        Next
                        .setCursor 14, 72
                        .SendKeyString SAMID ' Jeff`address samid for testing
                        .EnterKey
                        .FunctionKey_06
                        '**********ICMS S8 MEMO Below**********
                        .setCursor 3, 2 'Initial pointer posion in ICMS memo screen
                        list_index = 1
                        row_index = 3
                        For i = 3 To 21
                            .setCursor i, 2
                            If i = 3 Then
                                .SendKeyString "Chorus Portal ID: " & DATASHEET.Range("A" & rownumber).Value
                            Else
                                .SendKeyString MemoTxt_Arr(list_index)
                                list_index = list_index + 1
                                row_index = row_index + 1
                                If row_index = UBound(MemoTxt_Arr) - 1 Then Exit For
                                If row_index = 21 Then
                                    '.PageDown
                                    'row_index = 3
                                    Exit For
                                End If
                            End If
                        Next
                        .setCursor i + 1, 2
                        .SendKeyString "Product Plan: " & DATASHEET.Range("G" & rownumber).Value
                        '**********ICMS S8 MEMO Above**********
                        .FunctionKey_06
                        .setCursor 18, 64
                        .SendKeyString "17"
                        .EnterKey
                        employee = .scrRead(11, 30, 35)
                        .setCursor 14, 29
                        .SendKeyString employee
                        .setCursor 19, 29
                        .FunctionKey_08
                        .setCursor 19, 72
                        .FunctionKey_08
                        SO = .scrRead(5, 13, 19)
                        .EnterKey
                        .FunctionKey_01
                        .FunctionKey_03
                        .FunctionKey_03
                        .FunctionKey_03
                        '===Create S8 order Finish===
                    Else
                    Exit Function
                    End If
                Else
                    MsgBox "Not on WWC Menu"
                    Exit Function
                End If  '3.
            Else
                MsgBox "Return to Main Menu"
                Exit Function
            End If  '2.
        Else
        MsgBox "ICMS is not detected, please open ICMS FIRST"
        Exit Function
        End If  '1.
    End With
    CreateS8Order = SO
    'DATASHEET.Range("G" & rownumber).Value
    'POSTED
    Sleep 100
End Function

Function CreateX2Order(ByVal rownumber As Integer) As String
    Dim tnumber As String
    Dim Account As Long, asid As Long, list(100) As String, i As Integer, i_summary As Integer, i_F3 As Integer, SO As String, portalID As String
    Dim RFS As String, tomorrow As String, Day As String, month As String, Year As String, arr() As String
    Dim CPNumber As Long, OOTnumber As String
    Dim Plan As String, MsgRow As String, employee As String
    Dim icms As clsICMS
    
    tnumber = onlyDigits(DASHBOARD.Range("Q21").Value)
    portalID = DATASHEET.Range("A" & rownumber).Value
    Account = DATASHEET.Range("C" & rownumber).Value
    If InStr(DATASHEET.Range("B" & rownumber).Value, "Transfer") > 0 Then
        asid = DATASHEET.Range("W" & rownumber).Value
    Else
        asid = DATASHEET.Range("E" & rownumber).Value
    End If
    tomorrow = Format(Date, "ddmmyy") 'Actually this is value of today
    RFS = DATASHEET.Range("D" & rownumber).Value
    arr = Split(RFS, "/")
    Day = CStr(Trim(arr(0)))
    month = CStr(arr(1))
    Year = CStr(arr(2))
    Set icms = New clsICMS
    With icms
        If .ConnectPS = True Then '1. Check if ICMS Open
            If .onChorusMainMenu = True Then '2. Check if It`s on the Main Mnue
                .setCursor 24, 58
                .SendKeyString "01"
                .EnterKey
                If .onWWCMenu = True Then   '3.
                    For i = 1 To 2
                        .FunctionKey_05
                    Next
                    .setCursor 7, 7
                    .SendKeyString CStr(asid)
                    .EnterKey
                    MsgRow = .scrRead(24, 2, 44)
                    If InStr(MsgRow, "not found") > 0 Then
                        CreateX2Order = "Not Active ASID;No X2 created"
                        .FunctionKey_12
                        Exit Function
                    End If
                    .EnterKey
                    .setCursor 21, 19
                    .SendKeyString "04"
                    .EnterKey
                        .FunctionKey_06
                        If InStr(Trim(.scrRead(24, 2, 56)), "order type required") > 0 Then
                            DATASHEET.Range("L" & rownumber).Interior.Color = 255
                            DATASHEET.Range("Q" & rownumber).Interior.Color = 255
                            For i_F3 = 1 To 5
                                .FunctionKey_03
                            Next
                        End If
                        '===Create S8 order Start===
                        .setCursor 3, 29
                        .SendKeyString "X2"
                        .setCursor 4, 29
                        .FieldExitKey
                        .setCursor 4, 29
                        .SendKeyString Right(tnumber, 6)
                        .EnterKey
                        .EnterKey
                        .EnterKey
                        
                        .setCursor 5, 29
                        .setCursor 11, 35
                        .SendKeyString month
                        .setCursor 12, 73
                        .SendKeyString CStr(Day) & CStr(month) & Right(CStr(Year), 2) & "165901Y"
                        .FunctionKey_06
                        RFS = CStr(Day) & CStr(month) & Right(CStr(Year), 2)
                        If RFS <= tomorrow Or InStr(.scrRead(24, 3, 73), "less than the default") > 0 Then
                            .FunctionKey_14
                            .SendKeyString "t" & tnumber
                            .EnterKey
                        End If
                        '**********ICMS S8 MEMO Below**********
                        .setCursor 3, 2 'Initial pointer posion in ICMS memo screen
                        .SendKeyString "Order ID: " & DATASHEET.Range("A" & rownumber)
                        .setCursor 4, 2
                        .SendKeyString "Disconnect Date: " & DATASHEET.Range("D" & rownumber)
                        '**********ICMS S8 MEMO Above**********
                        .FunctionKey_06
                        .setCursor 18, 64
                        .SendKeyString "17"
                        .EnterKey
                        employee = .scrRead(11, 30, 35)
                        .setCursor 14, 29
                        .SendKeyString employee
                        .setCursor 19, 29
                        .FunctionKey_08
                        .setCursor 19, 72
                        .FunctionKey_08
                        SO = .scrRead(5, 13, 19)
                        .EnterKey
                        .FunctionKey_01
                        .FunctionKey_03
                        .FunctionKey_12

                        '===Create X2 order Finish===
                Else
                    MsgBox "Not on WWC Menu"
                    Exit Function
                End If  '3.
            Else
                MsgBox "Return to Main Menu"
                Exit Function
            End If  '2.
        Else
        MsgBox "ICMS is not detected, please open ICMS FIRST"
        Exit Function
        End If  '1.
    End With
    CreateX2Order = SO
End Function

Private Function FindRQedX2(ByVal asid As String, icms As clsICMS) As String
    Dim i As Integer, OrderStatus As String, orderType As String
    With icms
        .FunctionKey_05
        .FunctionKey_05
        .setCursor 7, 7
        .SendKeyString asid
        .setCursor 7, 18
        .SendKeyString "1"
        .EnterKey
        .EnterKey
        .setCursor 21, 19
        .SendKeyString "04"
        .EnterKey
        .setCursor 4, 25
        For i = 1 To 7
            .FieldExitKey
        Next
        .setCursor 5, 25
        .SendKeyString asid
        .setCursor 10, 68
        .SendKeyString "A"
        .EnterKey
        
        OrderStatus = .scrRead(7, 38, 43)
        If OrderStatus = "POSTED" Then
            FindRQedX2 = .scrRead(5, 13, 19)
            .FunctionKey_03
            .FunctionKey_12
            Exit Function
        Else
            For i = 8 To 19
                orderType = .scrRead(i, 40, 42)
                If orderType = "X2" Then
                    FindRQedX2 = .scrRead(i, 5, 11)
                    FindRQedX2 = Trim(FindRQedX2)
                    .FunctionKey_01
                    .FunctionKey_12
                    .FunctionKey_12
                    Exit For
                End If
            Next
        End If
    End With
End Function

Function initialize_icms() As Boolean
    Dim RumbaHwnd As Long, Retval As Long, rumbaapicontrol As Long, btnOK_RumbaAPIControl As Long, icms As New clsICMS
    Dim temp_var As String, cps As Boolean
    Dim winapi As New clsWinAPI
    If DASHBOARD.Range(modConstValues.icms_login_id_rg).Value = "" Or DASHBOARD.Range(modConstValues.icms_login_pass_rg).Value = "" Then
        initialize_icms = False
        Exit Function
    End If
    If icms.ConnectPS = False Then
        winapi.Sleeping 500
        OpenICMS
        Sleep 2000
    Else
        With icms
            cps = .ConnectPS
            If cps = True Then
                OpenICMS
            End If
        End With
    End If
    If icms.ConnectPS = True Then initialize_icms = True
    If icms.onChorusMainMenu = True Then
        initialize_icms = True
    Else
        OpenICMS
        Sleep 2000
    End If
End Function

Function OpenICMS() As Boolean
    Dim winapi As New clsWinAPI, AutoOpenICMS As Boolean
    Dim icms As New clsICMS, PSConnected As Boolean
    Dim i As Integer, i2 As Integer, i3 As Integer
    Dim NowHour As Integer, WaitTilSafeHour As Boolean: WaitTilSafeHour = False
    Dim arr As Variant, rsdaPath As String
    Dim ini As New clsINILoader
    AutoOpenICMS = False
    ini.iniFileLocation (modConstValues.config_file)
    If ini.getReturnValue("ICMS_SESSION", "ENABLE_MACRO_SESSION") = "TRUE" Then
        rsdaPath = ini.getReturnValue("ICMS_SESSION", "ICMS_MACRO_SESSION_PATH")
    Else
        rsdaPath = ini.getReturnValue("ICMS_SESSION", "ICMS_PRIMARY_SESSION_PATH")
    End If
    If icms.ConnectPS = False Then
        rsdaPath = "V:\Chorus Shared\Chorus\Customer Service\Provisioning\PBT_MACRO\settings\icms_session_for_macro.rsda"
        Shell "C:\Program Files (x86)\Micro Focus\RUMBA\System\RumbaPage.exe """ & rsdaPath & """", vbNormalNoFocus
        AutoOpenICMS = True
        winapi.Sleeping 10000
    End If
    With icms
        For i = 0 To 10
            PSConnected = .ConnectPS
            If PSConnected = True Then Exit For
            If i = 10 Then
                OpenICMS = False
                winapi.Sleeping 500
                Exit Function
            End If
            winapi.Sleeping 1000
        Next
        If AutoOpenICMS = True Then
            For i = 0 To 10
                If .scrRead(1, 36, 42) = "Sign On" Then Exit For
                If i = 10 Then
                    OpenICMS = False
                    winapi.Sleeping 500
                    Exit Function
                End If
                winapi.Sleeping 1000
            Next
        End If
        If .scrRead(1, 36, 42) = "Sign On" Then
            .setCursor 3, 24
            .SendKeyString DASHBOARD.Range(modConstValues.icms_login_id_rg).Value
            .setCursor 4, 24
            .SendKeyString DASHBOARD.Range(modConstValues.icms_login_pass_rg).Value
            .EnterKey
            winapi.Sleeping 1000
        End If
        If .scrRead(1, 33, 48) = "Display Messages" Then
            .FunctionKey_13
        End If
        If .scrRead(1, 33, 48) = "Display Messages" Then
            .FunctionKey_12
        End If
        If .scrRead(24, 11, 47) = "Password not correct for user profile" Then
            OpenICMS = False
            Exit Function
        End If
        If .scrRead(10, 24, 60) = "cannot sign on to more than 2 devices" Then
            .FunctionKey_06
        End If
        If InStr(.scrRead(1, 27, 52), "Display Program") > 0 Then
            .EnterKey
            Do While InStr(.scrRead(1, 27, 52), "Display Program") > 0
                DoEvents
            Loop
        End If
        If InStr(.scrRead(10, 11, 62), "already signed on at a different device") > 0 Then
            .FunctionKey_06
            Do While InStr(.scrRead(10, 11, 62), "already signed on at a different device") > 0
                DoEvents
            Loop
        End If
        If InStr(.scrRead(3, 20, 62), "Confidential Information Agreement") > 0 Then
            .setCursor 19, 12
            .SendKeyString "Y"
            .EnterKey
            Do While InStr(.scrRead(3, 20, 62), "Confidential Information Agreement") > 0
                DoEvents
            Loop
        End If
    End With
    OpenICMS = True
    Set winapi = Nothing
    Set icms = Nothing
    Set ini = Nothing
End Function
