Attribute VB_Name = "modMacroStatusIE"
Public status_ie As InternetExplorer

Sub init_StatusIE()
    Dim ini As New clsINILoader
    Dim winapi As New clsWinAPI, ie_chk As Long
    On Error Resume Next
    Err.Number = 1
    Do While Err.Number <> 0
        Err.Clear
        Set status_ie = New InternetExplorer
        winapi.Sleeping 400
    Loop
    On Error GoTo 0
    ini.iniFileLocation modConstValues.config_file
    For i = 1 To 15
        ie_chk = winapi.getObjectHwnd(vbNullString, "NGA Perform Billing Macro - Internet Explorer")
        If ie_chk = 0 Then Exit For
        If ie_chk <> 0 Then winapi.KillProcess ie_chk
        'SendMessage ie_chk, &H10, 0&, 0&
        winapi.Sleeping 50
        ie_chk = 0
    Next
    winapi.Sleeping 2000
    With status_ie
        .Visible = True
        .Toolbar = False
        .MenuBar = False
        .Resizable = False
        .Top = 0
        .Left = 0
        .Width = 400
        .Height = 220
        .Navigate ini.getReturnValue("STATUS_IE", "STATUS_IE_PATH")
        .Document.title = "NGA Perform Billing Macro"
    End With
    Set ini = Nothing
    Set winapi = Nothing
End Sub

Public Sub status_update(Optional TitleText As String, Optional H1Text As String, Optional PTag1Text As String, Optional PTag2Text As String)
    If TitleText <> "" Then
        status_ie.Document.title = TitleText
    End If
    If H1Text <> "" Then
        status_ie.Document.getElementById("atitle").innerText = H1Text
    End If
    If PTag1Text <> "" Then
        status_ie.Document.getElementById("totalrows").innerText = PTag1Text
    End If
    If PTag2Text <> "" Then
        status_ie.Document.getElementById("process_row").innerText = PTag2Text
    End If
End Sub
